﻿using System;
using System.Collections.Generic;

namespace BubbleSort
{
    public class Program
    {

        public List<int> BubbleSort(List<int> liste)
        {
            // bubbleSort hier
            
            
            return liste;
        }

        static void Main(string[] args)
        {
            Program prg = new Program();

            List<int> nichtSortierteListe = new List<int>() { 3, 5, 1, 2 }; // erzeugen einer unsortierten Liste 
            List<int> sortierteListe = prg.BubbleSort(nichtSortierteListe); // sortieren der Liste in der Funktion 
            for (int i = 0; i < sortierteListe.Count; i++)                  // Ausgabe der sortierten Liste
            {
                Console.WriteLine(sortierteListe[i]);
            }

            Console.ReadKey();
        }

    }
}
